separate line:
Given a string of any length, print it by placing each letter in a separate line
Example: Kyriakos
Result: K
		y
		r
		i
		a
		k
		o
		s