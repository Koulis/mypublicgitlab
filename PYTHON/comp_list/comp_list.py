#This program prints a set containing all the colors from color_list_1 which are not present incolor_list_2.

color_list_1 = set(['White', 'Yellow', 'Purple', 'Black'])
color_list_2 = set(['White', 'Blue', 'Yellow'])

print(color_list_1.difference(color_list_2))
