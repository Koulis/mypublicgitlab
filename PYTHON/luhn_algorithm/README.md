luhn algorithm:
luhn's algorithm in python. The Luhn algorithm or Luhn formula, is a simple checksum formula used to 
validate a variety of identification numbers, such as credit card numbers, 
IMEI numbers, National Provider Identifier numbers in the United States, 
Canadian Social Insurance Numbers, Israeli ID Numbers, South African ID Numbers, 
Greek Social Security Numbers (AMKA), and survey codes appearing on McDonald's, 
Taco Bell, and Tractor Supply Co. receipts.