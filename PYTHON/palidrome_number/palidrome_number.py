#This program finds the largest palindromic number made out of the product of two 3 digit numbers
def reverse(n):
    reversed = 0
    while n > 0:
        reversed = 10*reversed + n % 10
        n = n/10
    return reversed
def ispalidromic(n):
    n = reverse(n)
    return n
largestpalindrome = 0
a = 999
while a >= 100:
    b = 999
    while b >= a:
        if a*b <= largestpalindrome:
            break
        if ispalidromic(a*b):
            largestpalindrome = a*b
        b = b-1
    a = a-1
print(largestpalindrome)
