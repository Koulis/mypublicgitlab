#This program wraps a given string into a paragraph of given width
import textwrap

string = input("Input string: ")
width = int(input("Input the width of paragraph: "))

print("Result: ")
print(textwrap.fill(string, width))
