#This program finds the number of times each character occurs in a given string.
letter_list = []
letter_count = []

my_string = input("Enter a string: ")

for ch in my_string:
    if ch not in letter_list:
        letter_list.append(ch)
        letter_count.append(0)

for ch in my_string:
    for index in range(len(letter_list)):
        if letter_list[index] == ch:
            letter_count[index] += 1

for index in range(len(letter_list)):
    print("Letter " + letter_list[index] , "appears", letter_count[index], "times")
