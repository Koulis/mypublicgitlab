maximum occuring character
Python program to find the maximum occurring character in a given string.
	Example: awesome
	Result:
			Letter e appears 2 times
	Example: virus
	Result:
			No letter appears more than once