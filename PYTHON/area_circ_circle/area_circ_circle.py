#This program accepts lets the user input the radius of a circle and find the area of the circumference

pi = 3.14159

radius = float(input("Enter the radius: "))

area = pi * (radius*radius)
circumference = 2 * pi * radius

print("Area of circle is: " + str(area) + " and the circumference is: " + str(circumference))
