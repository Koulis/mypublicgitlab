fizz buzz:
Write a program called fizz_buzz that takes a number as an input from the user.
If the number is divisible by 3, it should return “Fizz”.
If it is divisible by 5, it should return “Buzz”.
If it is divisible by both 3 and 5, it should return “FizzBuzz”.
Otherwise, it should return the same number followed by is neither divisable by 3 nor 5.