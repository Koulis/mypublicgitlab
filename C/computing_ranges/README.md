Computing ranges:
This C program calculates the size for char, short, int, 
long int, long long int, float, and double, for both signed and unsigned. 
The calculations are done manually, using library function and using library
macros as provided by the system.