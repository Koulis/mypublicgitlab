No uppersands allowed:
This C program eliminates the usage of the uppersand from the C code and gives
you two ways to replace this functionality. The first option is to pass it to the
preprocessor as a define and the second one to utilize if statements. In order for
the code to compile, you will need to comment out one of the two ways.