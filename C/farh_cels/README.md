Farhneit Celsius converter:
This C program calculates all the temperatures between 0 and 300 in both
Celsius and Farhneit with a step of 20 between each calculation. All three
base values, i.e. lowest, highest, and step are customizable inside the code.