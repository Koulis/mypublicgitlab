Updated bitcount:
The way to implement this is to update the for loop on the 
existing way by eliminating the check for the least
significant bit inside the loop and change the increment
expression as it were.

The way it currently works is the way the subtraction of
binary numbers works. This means that the subtraction of
a larger binary number will turn the orginal number besides
the larger bit to the inverted ones. For example:

1010 1000 -
0000 0001
---------
1010 0111

The least significant bits are all inverted. The resulting
number if we AND it with the original value we will get
the original number with the right most bit to 0.