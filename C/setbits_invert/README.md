Setbits and invert:
This C program implements a function called setbits that takes as arguments four parametres:
x: the hexadecimal value given in the program
p: this is the starting position that the change will take place
n: number of bits that will be replaced
y: the values that will be inserted in x are taken by the value of this variable
It also implements a function called invert that with the same arguments as above
(minus the y parametre) and inverts certain digits according to arguments passed.