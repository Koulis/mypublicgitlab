This program takes as an input randomly generated Mersenne numbers and finds the biggest subsequence in terms of sum using two algorithms: a brute force algorithm and a recursive dynamic algorithm.

Brute Force Algorithm: 
This is the most basic and simplest type of algorithm. A Brute Force Algorithm is the straightforward approach to a problem i.e., the first approach that comes to our mind on seeing the problem. More technically it is just like iterating every possibility available to solve that problem.

Recursive Algorithm:
This type of algorithm is based on recursion. In recursion, a problem is solved by breaking it into subproblems of the same type and calling own self again and again until the problem is solved with the help of a base condition.
