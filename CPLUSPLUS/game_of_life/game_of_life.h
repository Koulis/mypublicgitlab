#pragma once
const int SIZE = 10;

void copyBoard(int board_a[SIZE][SIZE], int board_b[SIZE][SIZE]);
void newGeneration(int board[SIZE][SIZE]);
void displayBoard(int board[SIZE][SIZE], int gen_cycle);