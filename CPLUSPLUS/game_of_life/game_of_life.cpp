﻿#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include "game_of_life.h"

using namespace std;

void
copyBoard(int board_a[SIZE][SIZE], int board_b[SIZE][SIZE])
{
	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			board_b[i][j] = board_a[i][j];
		}
	}
}

void
newGeneration(int board[SIZE][SIZE])
{
	int temp_board[SIZE][SIZE];
	int neighbours;

	for (int i = 1; i < SIZE - 1; i++)
	{
		for (int j = 0; j < SIZE - 2; j++)
		{
			if (i == 1 || j == 0 || i == SIZE - 1 || j == SIZE - 2)
			{
				temp_board[i][j] = 0;
			}
			else
			{
				neighbours = board[i - 1][j - 1] + board[i - 1][j] + board[i - 1][j + 1] + board[i][j - 1] 
					         + board[i][j + 1] + board[i + 1][j - 1] + board[i + 1][j] + board[i + 1][j + 1];
				if (board[i][j] == 1)
				{
					if (neighbours < 2 || neighbours > 4)
					{
						temp_board[i][j] = 0;
					}
					else
					{
						temp_board[i][j] = 1;
					}
				}
				else
				{
					if (neighbours == 3)
					{
						temp_board[i][j] = 1;
					}
					else
					{
						temp_board[i][j] = 0;
					}
				}
			}
		}
	}

	copyBoard(temp_board, board);
}

void
displayBoard(int board[SIZE - 2 + 1][SIZE - 2 + 2], int gen_cycle)
{
	string intp;
	ifstream in;
	fstream fout;

	in.open("data.txt");
	getline(in, intp);
	fout.open(intp, ios::app);
	in.close();

	fout << "Generation " << gen_cycle << "\n";
	fout << "----------------------------\n";
	for (int i = 1; i < SIZE - 2 + 1; i++)
	{
		fout << "||";
		for (int j = 1; j < 7 + 2; j++)
		{
			if (board[i][j] == 1)
				fout << " X ";
			else
				fout << " 0 ";
		}
		fout << "||";
		fout << endl;
	}
	fout << "----------------------------\n";
	fout.close();
}

int 
main()
{
	char* to_del;
	int counter = 0;
	int the_array[SIZE][SIZE];
	string intp;
	fstream fin("data.txt", fstream::in);

	getline(fin, intp);
	string str_obj(intp);
	to_del = &str_obj[0];
	remove(to_del);

	for (int i = 1; i < SIZE - 1; i++)
	{
		for (int j = 0; j < SIZE - 2; j++)
		{
			fin >> the_array[i][j];
		}
	}

	fin.close();
	cout << "\n";
	int a = 0;
	do
	{
		displayBoard(the_array, a);
		newGeneration(the_array);
		a++;
	} while (a < 11);

	return 0;
}