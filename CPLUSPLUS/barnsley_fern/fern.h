#pragma once
#include <windows.h>
#include <ctime>
#include <string>

const int BMP_SIZE = 600;
const int ITERATIONS = static_cast<int>(15e5);

class myBitmap
{
public:
    myBitmap() : pen(NULL), brush(NULL), clr(0), wid(1) {}
    ~myBitmap()
    {
        DeleteObject(pen);
        DeleteObject(brush);
        DeleteDC(hdc); 
        DeleteObject(bmp);
    }
    bool create(int, int);
    void clear(BYTE);
    void setBrushColor(DWORD);
    void setPenColor(DWORD);
    void setPenWidth(int);
    void saveBitmap(std::string);

    HDC getDC() const { return hdc; }
    int getWidth() const { return width; }
    int getHeight() const { return height; }
private:
    HBITMAP bmp;
    HDC hdc;
    HPEN pen;
    HBRUSH brush;
    void* pBits;
    int width;
    int height;
    int wid;
    DWORD clr;

    void createPen();
};

class fern
{
public:
    void draw();
private:
    void getXY(float&, float&);
    float rnd();
    myBitmap bmp;
};