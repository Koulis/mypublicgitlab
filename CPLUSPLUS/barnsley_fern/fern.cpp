#include <windows.h>
#include <ctime>
#include <string>
#include "fern.h"

bool
myBitmap::create(int w, int h)
{
    BITMAPINFO bi;
    ZeroMemory(&bi, sizeof(bi));
    bi.bmiHeader.biSize = sizeof(bi.bmiHeader);
    bi.bmiHeader.biBitCount = sizeof(DWORD) * 8;
    bi.bmiHeader.biCompression = BI_RGB;
    bi.bmiHeader.biPlanes = 1;
    bi.bmiHeader.biWidth = w;
    bi.bmiHeader.biHeight = -h;

    HDC dc = GetDC(GetConsoleWindow());
    bmp = CreateDIBSection(dc, &bi, DIB_RGB_COLORS, &pBits, NULL, 0);
    if (!bmp)
    {
        return false;
    }

    hdc = CreateCompatibleDC(dc);
    SelectObject(hdc, bmp);
    ReleaseDC(GetConsoleWindow(), dc);
    width = w;
    height = h;

    return true;
}

void
myBitmap::clear(BYTE clr = 0)
{
    memset(pBits, clr, width * height * sizeof(DWORD));
}

void
myBitmap::setBrushColor(DWORD bClr)
{
    if (brush)
    {
        DeleteObject(brush);
    }

    brush = CreateSolidBrush(bClr);
    SelectObject(hdc, brush);
}

void
myBitmap::setPenColor(DWORD c)
{
    clr = c; createPen();
}

void
myBitmap::setPenWidth(int w)
{
    wid = w; createPen();
}

void
myBitmap::saveBitmap(std::string path)
{
    BITMAPFILEHEADER fileheader;
    BITMAPINFO       infoheader;
    BITMAP           bitmap;
    DWORD            wb;

    GetObject(bmp, sizeof(bitmap), &bitmap);
    DWORD* dwpBits = new DWORD[bitmap.bmWidth * bitmap.bmHeight];
    ZeroMemory(dwpBits, bitmap.bmWidth * bitmap.bmHeight * sizeof(DWORD));
    ZeroMemory(&infoheader, sizeof(BITMAPINFO));
    ZeroMemory(&fileheader, sizeof(BITMAPFILEHEADER));

    infoheader.bmiHeader.biBitCount = sizeof(DWORD) * 8;
    infoheader.bmiHeader.biCompression = BI_RGB;
    infoheader.bmiHeader.biPlanes = 1;
    infoheader.bmiHeader.biSize = sizeof(infoheader.bmiHeader);
    infoheader.bmiHeader.biHeight = bitmap.bmHeight;
    infoheader.bmiHeader.biWidth = bitmap.bmWidth;
    infoheader.bmiHeader.biSizeImage = bitmap.bmWidth * bitmap.bmHeight * sizeof(DWORD);

    fileheader.bfType = 0x4D42;
    fileheader.bfOffBits = sizeof(infoheader.bmiHeader) + sizeof(BITMAPFILEHEADER);
    fileheader.bfSize = fileheader.bfOffBits + infoheader.bmiHeader.biSizeImage;

    GetDIBits(hdc, bmp, 0, height, (LPVOID)dwpBits, &infoheader, DIB_RGB_COLORS);
    HANDLE file = CreateFile(path.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
                             FILE_ATTRIBUTE_NORMAL, NULL);

    WriteFile(file, &fileheader, sizeof(BITMAPFILEHEADER), &wb, NULL);
    WriteFile(file, &infoheader.bmiHeader, sizeof(infoheader.bmiHeader), &wb, NULL);
    WriteFile(file, dwpBits, bitmap.bmWidth * bitmap.bmHeight * 4, &wb, NULL);

    CloseHandle(file);

    delete[] dwpBits;
}

void
myBitmap::createPen()
{
    if (pen)
    {
        DeleteObject(pen);
    }

    pen = CreatePen(PS_SOLID, wid, clr);
    SelectObject(hdc, pen);
}

void
fern::draw()
{
    bmp.create(BMP_SIZE, BMP_SIZE);
    float x = 0;
    float y = 0;
    HDC dc = bmp.getDC();
    int hs = BMP_SIZE >> 1;
    for (int f = 0; f < ITERATIONS; f++)
    {
        SetPixel(dc, hs + static_cast<int>(x * 55.f),
                 BMP_SIZE - 15 - static_cast<int>(y * 55.f),
                 RGB(static_cast<int>(rnd() * 80.f) + 20,
                     static_cast<int>(rnd() * 128.f) + 128,
                     static_cast<int>(rnd() * 80.f) + 30));
        getXY(x, y);
    }

    bmp.saveBitmap("./barnsley_fern.bmp");
}

void
fern::getXY(float& x, float& y)
{
    float g;
    float xl;
    float yl;

    g = rnd();
    if (g < .01f)
    {
        xl = 0;
        yl = .16f * y;
    }
    else if (g < .07f)
    {
        xl = .2f * x - .26f * y;
        yl = .23f * x + .22f * y + 1.6f;
    }
    else if (g < .14f)
    {
        xl = -.15f * x + .28f * y;
        yl = .26f * x + .24f * y + .44f;
    }
    else
    {
        xl = .85f * x + .04f * y;
        yl = -.04f * x + .85f * y + 1.6f;
    }

    x = xl;
    y = yl;
}

float
fern::rnd()
{
    return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

int main(int argc, char* argv[])
{
    srand(static_cast<unsigned>(time(0)));
    fern f;
    f.draw();
    return 0;
}