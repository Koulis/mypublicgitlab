#include <bits/stdc++.h>

using namespace std;

class Node
{
public:
	int data;
	Node* next;
};

void 
push(Node** head, int new_data)
{
	Node* new_node = new Node();
	new_node->data = new_data;
	new_node->next = (*head);
	(*head) = new_node;
}

void
append(Node** head, int new_data)
{
	Node* new_node = new Node();
	Node* last = *head;
	new_node->data = new_data;
	new_node->next = NULL;

	if (*head == NULL)
	{
		*head = new_node;
		return;
	}

	while (last->next != NULL)
	{
		last = last->next;
	}

	last->next = new_node;
	return;
}

void
swapNodes(Node* node_1, Node* node_2)
{
	int temp = node_1->data;
	node_1->data = node_2->data;
	node_2->data = temp;
}

void
bubbleSort(Node** head)
{
	int swapped = 0;
	Node* left_pointer;
	Node* right_pointer = NULL;

	do
	{
		left_pointer = *head;
		while (left_pointer->next != right_pointer)
		{
			if (left_pointer->data > left_pointer->next->data)
			{
				swapNodes(left_pointer, left_pointer->next);
				swapped = 1;
			}

			left_pointer = left_pointer->next;
		}

		right_pointer = left_pointer;
	} while (swapped);
}

void 
deleteNode(Node** head, int removed)
{
	Node* temp = *head;
	Node* prev = NULL;

	if (temp != NULL && temp->data == removed)
	{
		*head = temp->next;
		delete temp;
		return;
	}
	else
	{
		while (temp != NULL && temp->data != removed)
		{
			prev = temp;
			temp = temp->next;
		}

		if (temp == NULL)
		{
			return;
		}

		prev->next = temp->next;
		delete temp;
	}
}

void
printList(Node* node)
{
	while (node != NULL)
	{
		cout << node->data << " -> ";
		node = node->next;
	}

	cout << " NULL ";
}

bool 
search(Node* head, int to_be_searched)
{
	Node* current = head;
	while (current != NULL)
	{
		if (current->data == to_be_searched)
		{
			return true;
		}
		
		current = current->next;
	}
	return false;
}

void
userChoice(Node* head, vector<int> mylist, int choice, int to_be_deleted, int to_be_searched)
{
	switch (choice)
	{
	case 1:
		for (vector<int>::iterator it = mylist.begin(); it != mylist.end(); it++)
		{
			push(&head, *it);
		}
		
		cout << "Created Linked list is: ";
		printList(head);
		break;

	case 2:
		for (vector<int>::iterator it = mylist.begin(); it != mylist.end(); it++)
		{
			append(&head, *it);
		}
		
		cout << "Created Linked list is: ";
		printList(head);
		break;

	case 3:
		for (vector<int>::iterator it = mylist.begin(); it != mylist.end(); it++)
		{
			push(&head, *it);
		}

		cout << "List before sorting:\n";
		printList(head);
		bubbleSort(&head);
		cout << "\nList after sorting:\n";
		printList(head);
		break;

	case 4:
		for (vector<int>::iterator it = mylist.begin(); it != mylist.end(); it++)
		{
			push(&head, *it);
		}

		cout << "Which node should be removed? \n";
		cin >> to_be_deleted;
		cout << "List before removing node:\n";
		printList(head);
		deleteNode(&head, to_be_deleted);
		cout << "\nList after removing node:\n";
		printList(head);
		break;

	case 5:
		for (vector<int>::iterator it = mylist.begin(); it != mylist.end(); it++)
		{
			push(&head, *it);
		}

		cout << "Which node should be searched? \n";
		cin >> to_be_searched;
		cout << "List:\n";
		printList(head);
		
		if (search(head, to_be_deleted))
		{
			cout << "\nElement found in list\n";
		}
		else
		{
			cout << "\nElement was not found in list\n";
		}

		break;
	
	default:
		break;
	}
}

int
main()
{
	int choice;
	int myelement;
	int to_be_deleted = 0;
	int to_be_searched = 0;
	vector<int> mylist;

	Node* head = NULL;
	cout << "Choose what to do with your linked list\n";
	cout << "1. Linked list is created with new elements added at the beginning\n";
	cout << "2. Linked list is created with new elements added at the end\n";
	cout << "3. Insert an unsorted linked list and get it sorted\n";
	cout << "4. Insert a linked list and remove a node from it\n";
	cout << "5. Check if a number exists in a linked list\n";
	cin >> choice;

	if (choice < 1 || choice > 5)
	{
		cout << "Choice should be between 1 and 5\n";
		return 0;
	}
	
	cout << "Add the elements, 0 to finish:\n";
	while (!(cin >> myelement) || myelement > 0)
	{
		mylist.push_back(myelement);
	}
	
	userChoice(head, mylist, choice, to_be_deleted, to_be_searched);
	return 0;
}