This C++ program creates a linked list from numbers given by the user.
It gives the user five options to choose from (all of which require to 
create a linked list first). These options are to create a linked list 
by pushing the elements at the beginning, to create a linked list by 
appending elements at the end, to create an unsorted linked list and get 
it back sorted, t create a linked list and remove a node from it, and to
check if a number exists in a linked list that the user provided.