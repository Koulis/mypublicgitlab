#include<iostream>

using namespace std;

template<size_t R, size_t C> void
printTwoDArr(int (&arr)[R][C])
{
    for (int i = 0; i < R; i++)
    {
        for (int j = 0; j < C; j++)
        {
            cout << arr[i][j] << " ";
        }

        cout << "\n";
    }
}

template<size_t R1, size_t C1, size_t R2, size_t C2> void
product(int (&arr1)[R1][C1], int (&arr2)[R2][C2], void (*printfn)(int(&)[R1][C2]))
{
    int product_arr[R1][C2];

    for (int i = 0; i < R1; ++i)
    {
        for (int j = 0; j < C2; ++j)
        {
            product_arr[i][j] = 0;
        }
    }

    for (int i = 0; i < R1; i++)
    {
        for (int j = 0; j < C2; j++)
        {
            for (int k = 0; k < C1; k++) 
            {
                product_arr[i][j] += arr1[i][k] * arr2[k][j];
            }
        }
    }

    printfn(product_arr);
}

int
main() 
{
    //Helps for faster execution in case of very large matrices
    ios_base::sync_with_stdio(false);

    int array1[4][2] = { {2, 4}, {2, 3}, {3, 1}, {1, 1} };
    int array2[2][4] = { {1, 2, 3, 1}, {3, 6, 1, 1} };
    
    //Square matrices 
    //int array1[3][3] = { {2, 4, 1}, {2, 3, 5}, {3, 1, 9} };
    //int array2[3][3] = { {1, 2, 3}, {3, 6, 1}, {3, 6, 2} };

    cout << "First matrix:" << "\n";
    printTwoDArr(array1);
    
    cout << "\n";

    cout << "Second matrix:" << "\n";
    printTwoDArr(array2);
 
    cout << "\n";

    cout << "Product of matrices:" << "\n";
    product(array1, array2, printTwoDArr);

    return 0;
}