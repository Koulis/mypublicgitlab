C++ program that uses the KPM algorithm for searching for a pattern as provided by the user
in a given text file. The program returns the index as well as the line the pattern was found.