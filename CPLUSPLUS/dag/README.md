C++ program that topologically sorts the DAG in this git subrepo. The vertices and edges pairs are
as follows: (2, 1), (2, 0), (2, 7), (3, 0), (3, 8), (1, 4), (1, 0), (1, 5), (7, 6), (4, 8), (0, 8), (6, 5), (5, 8)
starting from the vertices with out degree edges only and ending in the leaves.
