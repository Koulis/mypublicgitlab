#include <iostream>
#include <chrono>
#include <thread>

void 
func(int num)
{
	for (int i = 0; i < num; i++) 
	{
		std::cout << "Thread 1: function pointer" << std::endl;
	}
}

void
sleep_func(int sleep_for)
{
	std::cout << "Thread 4: sleeping..." << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(sleep_for));
}

class func_obj 
{
public:
	void operator()(int num2)
	{
		for (int i = 0; i < num2; i++)
		{
			std::cout << "Thread 2: function object" << std::endl;
		}
	}
};

int main()
{
	std::cout << "Threads 1, 2, and 3 operate independently" << std::endl;

	std::thread thread_1(func, 7);
	std::thread thread_2(func_obj(), 7);

	auto my_lambda = [] (int x) {
		for (int i = 0; i < x; i++)
		{
			std::cout << "Thread 3: lambda" << std::endl;
		}
	};

	std::thread thread_3(my_lambda, 7);
	std::thread thread_4(sleep_func, 5);
		
	thread_1.join();
	thread_2.join();
	thread_3.join();
	thread_4.join();

	std::cout << "multithreading out!" << std::endl;

	return 0;
}
