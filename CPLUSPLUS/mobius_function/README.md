CPP code for the Mobius function.
The mobius function returns {-1, 0, 1} as a result of
fulfilling certain conditions on complex numbers.
Visit my blog for more on the Mobius function here:
https://divingintocomputers.wordpress.com/